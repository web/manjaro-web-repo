#!/usr/bin/env python3

MIRRORS_URL = "https://repo.manjaro.org/mirrors.json"
BRANCHES = ("stable", "testing", "unstable")
ROOT_FOLDER = "/var/www/manjaro-web-repo/"
OUTPUT_FOLDER = "docs/"
LOGS_FOLDER = "logs/"
UA = "ManjaroMirrorBot/1.0 (+http://repo.manjaro.org)"
